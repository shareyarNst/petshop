<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Response Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during responses for various
    | messages that we need to display to the user. 
    |
    */

    'validation'        => 'Validation Error.',
    'unauthorized'      => 'Unauthorized.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'success'           => 'Request successfull.',
    'failed'            => 'Request failed.',
    'auth_failed'       => 'Invalid credentials.',
    'logout'            => 'Logout successful',
    'delete'            => 'Record deleted.',
    'no_record'         => "No record found with given :attribute.",

    'token' => [
        'missing' => 'Token is missing.',
        'invalid' => 'Token is invalid.',
        'expired' => 'Token is expired.'
    ]

];
