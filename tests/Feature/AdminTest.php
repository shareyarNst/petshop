<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    public function testMustEnterEmailAndPassword()
    {
        $this->postJson('api/v1/admin/login')
            ->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The email field is required.",
                    "The password field is required.",
                ]
            ]);
    }

    public function testAdminLogin()
    {
        $email = 'admin@buckhill.co.uk';
        $user = User::factory(1)->create([
            'email' => $email, 
            'password' => '$2y$10$qBRwwVqoSzRFirbvcqfXuORWzQt7LA/qL1zdSxX0lfmmoM7YOfzYG',
            'is_admin' => true
        ]);

        $response = $this->postJson('/api/v1/admin/login', [
            'email' => $email,
            'password' => 'admin'
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token' ]]);
    }

    public function testAdminCannotLogoutWithoutToken()
    {
        $logoutResponse = $this->get('/api/v1/admin/logout');
        $logoutResponse->assertStatus(401);
    }

    public function testAdminLogoutSuccessfl()
    {
        $email = 'admin@buckhill.co.uk';
        $user = User::factory(1)->create([
            'email' => $email,
            'password' => '$2y$10$qBRwwVqoSzRFirbvcqfXuORWzQt7LA/qL1zdSxX0lfmmoM7YOfzYG',
            'is_admin' => true
        ]);

        $response = $this->postJson('/api/v1/admin/login', [
            'email' => $email,
            'password' => 'admin'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $logoutResponse = $this->withHeaders([
            'Authorization' => "Bearer ".$token,

        ])->get('/api/v1/admin/logout');


        $logoutResponse->assertStatus(200);
    }

    public function testOnlyAdminUserLogin()
    {
        $email = 'user@mail.com';
        $user = User::factory(1)->create(['email' => $email]);

        $response = $this->postJson('/api/v1/admin/login', [
            'email' => $email,
            'password' => 'password'
        ]);

        $response->assertStatus(401);
    }
    

    public function testCreateAdminUserMustHaveAllFields()
    {
        $this->postJson('api/v1/admin/create')
            ->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The first name field is required.",
                    "The last name field is required.",
                    "The email field is required.",
                    "The password field is required.",
                    "The avatar field is required.",
                    "The address field is required.",
                    "The phone number field is required."
                ]
            ]);
    }

    public function testCreateAdminMustHavePasswordConfirmation()
    {

        $adminEmail = 'admin@buckhil.com.uk';

        $response = $this->postJson('/api/v1/admin/create', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $adminEmail,
            'password' => 'password',
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => 'a7fa7e1a-da55-4d29-ab63-182e31aa937f'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The password confirmation does not match."
                ]
            ]);
    }

    public function testCreateAdminUserSuccessful()
    {

        $adminEmail = 'admin@buckhil.com.uk';

        $response = $this->postJson('/api/v1/admin/create', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $adminEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(200)
            ->assertJson(['data' => ['user' => ['email' => $adminEmail, 'is_admin' => 1]]]);
    }
}
