<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testUserMustEnterEmailAndPassword()
    {
        $this->postJson('api/v1/user/login')
            ->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The email field is required.",
                    "The password field is required.",
                ]
            ]);
    }

    public function testUserLoginSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']]);
    }

    public function testAdminCannotLogoutWithoutToken()
    {
        $logoutResponse = $this->get('/api/v1/user/logout');
        $logoutResponse->assertStatus(401);
    }

    public function testUserLogoutSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $logoutResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,

        ])->get('/api/v1/user/logout');


        $logoutResponse->assertStatus(200);
    }

    public function testOnlyUserCanLogin()
    {
        $email = 'admin@buckhill.co.uk';
        $user = User::factory(1)->create(['email' => $email, 'is_admin' => true])->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $response->assertStatus(401);
    }


    public function testCreateUserMustHaveAllFields()
    {
        $this->postJson('api/v1/user/create')
            ->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The first name field is required.",
                    "The last name field is required.",
                    "The email field is required.",
                    "The password field is required.",
                    "The avatar field is required.",
                    "The address field is required.",
                    "The phone number field is required."
                ]
            ]);
    }

    public function testCreateAdminMustHavePasswordConfirmation()
    {
        $userEmail = 'user@buckhil.com.uk';

        $response = $this->postJson('/api/v1/user/create', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $userEmail,
            'password' => 'password',
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => 'a7fa7e1a-da55-4d29-ab63-182e31aa937f'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The password confirmation does not match."
                ]
            ]);
    }

    public function testCreateUserSuccessful()
    {
        Storage::fake('avatars');

        $userEmail = 'user@buckhil.com.uk';

        $response = $this->postJson('/api/v1/user/create', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $userEmail,
            'password' => 'password',
            'password_confirmation' => 'password',
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(200)
            ->assertJson(['data' => ['user' => ['email' => $userEmail, 'is_admin' => 0]]]);
    }

    public function testUpdateUserMustHaveAuthToken()
    {
        $this->putJson('api/v1/user/edit')->assertStatus(401);
    }

    public function testUpdateUserMustHaveAllFields()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,

        ])->putJson('api/v1/user/edit');

        $updateResponse->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The first name field is required.",
                    "The last name field is required.",
                    "The email field is required.",
                    "The address field is required.",
                    "The phone number field is required."
                ]
            ]);
    }


    public function testUpdateUserAvatarMustExists()
    {
        $user = User::factory(1)->create()->first();
        $updateEmail = "johndoe@buchkill.co.uk";

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,

        ])->putJson('api/v1/user/edit', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $updateEmail,
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => 'a7fa7e1a-da55-4d29-ab63-182e31aa937f'
        ]);

        $updateResponse->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The selected avatar is invalid.",
                ]
            ]);
    }

    public function testUpdateUserAvatarSuccessfull()
    {
        $user = User::factory(1)->create()->first();
        $updateEmail = "johndoe@buchkill.co.uk";

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $fileUploadResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.jpg')
            ]);

        $fileUploadResponse = $fileUploadResponse->assertStatus(200)
            ->assertJsonStructure(['data' => ['file']])->decodeResponseJson();
        
        
        $fileUUID = $fileUploadResponse['data']['file']['uuid'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,

        ])->putJson('api/v1/user/edit', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $updateEmail,
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => $fileUUID
        ]);

        $updateResponse->assertStatus(200);
    }

    public function testUpdateUserSuccessful()
    {
        $user = User::factory(1)->create()->first();
        $updateEmail = "johndoe@buchkill.co.uk";

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,
        ])->putJson('api/v1/user/edit', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $updateEmail,
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
        ]);

        $updateResponse->assertStatus(200)
            ->assertJson([
                "data" => [
                    "user" => [
                        "email" => $updateEmail
                    ]
                ]
            ]);
    }

    public function testUpdateUserPasswordNeedPasswordConfirmation()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,
        ])->putJson('api/v1/user/edit', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $user->email,
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'avatar' => 'a7fa7e1a-da55-4d29-ab63-182e31aa937f',
            'password' => 'userpasswordupdate',
        ]);

        $updateResponse->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The password confirmation does not match.",
                ]
            ]);
    }

    public function testUpdateUserPasswordSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,
        ])->putJson('api/v1/user/edit', [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => $user->email,
            'address' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'phone_number' => '+15639882660',
            'password' => 'userpasswordupdate',
            'password_confirmation' => 'userpasswordupdate',
        ]);

        $updateResponse->assertStatus(200);
    }

    public function testGetAuthenticatedUser()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $updateResponse = $this->withHeaders([
            'Authorization' => "Bearer " . $token,
        ])->get('api/v1/user');

        $updateResponse->assertStatus(200)
            ->assertJson([
                "data" => [
                    "user" => [
                        "email" => $user->email
                    ]
                ]
            ]);
    }

    public function testUserForgotPasswordMustHaveEmail()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/forgot-password');

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The email field is required.",
                ]
            ]);
    }

    public function testUserForgotPasswordSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/forgot-password', [
            'email' => $user->email
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    'reset-token'
                ]
            ]);
    }

    public function testUserResetPasswordTokenMustHaveAllFields()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/reset-password-token');

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The token field is required.",
                    "The password field is required.",
                ]
            ]);
    }

    public function testUserResetPasswordTokenMustHavePasswordConfirmation()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/forgot-password', [
            'email' => $user->email
        ]);

        $responseData = $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    'reset-token'
                ]
            ])->decodeResponseJson();

        $resetToken = $responseData['data']['reset-token'];

        $response = $this->postJson('/api/v1/user/reset-password-token', [
            'token' => $resetToken,
            'password' => 'newpassword'
        ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The password confirmation does not match.",
                ]
            ]);
    }

    public function testUserResetPasswordTokenMustSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/forgot-password', [
            'email' => $user->email
        ]);

        $responseData = $response->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    'reset-token'
                ]
            ])->decodeResponseJson();

        $resetToken = $responseData['data']['reset-token'];

        $response = $this->postJson('/api/v1/user/reset-password-token', [
            'token' => $resetToken,
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword',
        ]);

        $response->assertStatus(200)
            ->assertJson([
                "message" => "Request successfull.",
            ]);
    }
}
