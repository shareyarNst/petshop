<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FileTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUploadFileNeedAuthUser()
    {
        $response = $this->postJson('/api/v1/file/upload');

        $response->assertStatus(401);
    }


    public function testUploadFileMustHaveRequiredFields()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload');

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The file field is required.",
                ]
            ]);
    }

    public function testUploadFileMustHaveValidImageFile()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => 'Lorem ipsum'
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The file must be an image.",
                ]
            ]);
    }

    public function testUploadFileMustBePngJpgJpegType()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.svg')
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The file must be a file of type: jpg, png, jpeg."
                ]
            ]);
    }

    public function testUploadFileCannotGreaterThanOneMb()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.jpg')->size(1025)
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The file must not be greater than 1024 kilobytes."
                ]
            ]);
    }

    public function testUploadFileSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.jpg')
            ]);

        $response->assertStatus(200)
            ->assertJson([
                "message" => "Request successfull.",
            ]);
    }
}
