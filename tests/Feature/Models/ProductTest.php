<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\User;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateProductMustHaveAuthUser()
    {
        $response = $this->postJson('/api/v1/product/create');

        $response->assertStatus(401);
    }

    public function testCreateProductMustHaveAllFields()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/product/create');

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The category uuid field is required.",
                    "The title field is required.",
                    "The description field is required.",
                    "The price field is required.",
                    "The metadata field is required."
                ]
            ]);
    }

    public function testCreateProductSelectedCategoryMustExists()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/product/create', [
                'category_uuid' => '654f290b-86f5-403d-96cd-257cd042809d',
                'title' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'price' => 100,
                'metadata' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The selected category uuid is invalid.",
                    "The metadata must be a valid JSON string."
                ]
            ]);
    }

    public function testCreateProductMetadataFieldMustBeAValidJson()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];


        $category = Category::factory(1)->create()->first();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/product/create', [
                'category_uuid' => $category->uuid,
                'title' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'price' => 100,
                'metadata' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The metadata must be a valid JSON string."
                ]
            ]);
    }

    public function testCreateProductSelectedBrandMustExists()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];


        $category = Category::factory(1)->create()->first();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/product/create', [
                'category_uuid' => $category->uuid,
                'title' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'price' => 100,
                'metadata' => json_encode([
                    'brand' => '654f290b-86f5-403d-96cd-257cd042809d',
                    'image' => '654f290b-86f5-403d-96cd-257cd042809d'
                ])
            ]);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The selected brand is invalid."
                ]
            ]);
    }

    public function testCreateProductSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $fileUploadResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.jpg')
            ]);

        $fileUploadResponse = $fileUploadResponse->assertStatus(200)
            ->assertJsonStructure(['data' => ['file']])->decodeResponseJson();

        $category = Category::factory(1)->create()->first();
        $brand    = Brand::factory(1)->create()->first();
        $fileUUID = $fileUploadResponse['data']['file']['uuid'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/product/create', [
                'category_uuid' => $category->uuid,
                'title' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'price' => 100,
                'metadata' => json_encode([
                    'brand' => $brand->uuid,
                    'image' => $fileUUID
                ])
            ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "message",
                "data" => [
                    'product'
                ]
            ]);
    }

    public function testUpdateProductMustHaveAuthUser()
    {
        $product = Product::factory(1)->create()->first();

        $response = $this->putJson('/api/v1/product/' . $product->uuid);

        $response->assertStatus(401);
    }

    public function testUpdateProductMustHaveAllFields()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $product = Product::factory(1)->create()->first();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->putJson("/api/v1/product/" . $product->uuid);

        $response->assertStatus(422)
            ->assertJson([
                "message" => "Validation Error.",
                "errors" => [
                    "The category uuid field is required.",
                    "The title field is required.",
                    "The description field is required.",
                    "The price field is required.",
                    "The metadata field is required."
                ]
            ]);
    }

    public function testUpdateProductSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        Storage::fake('avatars');

        $fileUploadResponse = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->postJson('/api/v1/file/upload', [
                'file' => UploadedFile::fake()->image('avatar.jpg')
            ]);

        $fileUploadResponse = $fileUploadResponse->assertStatus(200)
            ->assertJsonStructure(['data' => ['file']])->decodeResponseJson();

        $category = Category::factory(1)->create()->first();
        $brand    = Brand::factory(1)->create()->first();
        $product  = Product::factory(1)->create()->first();
        $fileUUID = $fileUploadResponse['data']['file']['uuid'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->putJson("/api/v1/product/" . $product->uuid, [
                'category_uuid' => $category->uuid,
                'title' => 'Product One',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'price' => 100,
                'metadata' => json_encode([
                    'brand' => $brand->uuid,
                    'image' => $fileUUID
                ])
            ]);

        $response->assertStatus(200)
            ->assertJson([
                "message" => "Request successfull.",
                "data" => [
                    "product" => [
                        "uuid" => $product->uuid,
                        'category_uuid' => $category->uuid,
                        "title" => "Product One",
                        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                        'price' => 100,
                        'metadata' => [
                            'brand' => $brand->uuid,
                            'image' => $fileUUID
                        ]
                    ]
                ]
            ]);
    }

    public function testDeleteProductSuccessful()
    {
        $user = User::factory(1)->create()->first();

        $response = $this->postJson('/api/v1/user/login', [
            'email' => $user->email,
            'password' => 'userpassword'
        ]);

        $data = $response->assertStatus(200)
            ->assertJsonStructure(['data' => ['token']])->decodeResponseJson();

        $token = $data['data']['token'];

        $product = Product::factory(1)->create()->first();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->delete("/api/v1/product/" . $product->uuid);

        $response->assertStatus(200);
    }

}
