<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\PromotionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1/admin',
],function()
{
    Route::post('/login',[AdminController::class,'login']);
    Route::post('/create', [AdminController::class, 'store']);

    Route::group(['middleware' => ['authToken','isAdmin']],function(){
        Route::get('/logout', [AdminController::class, 'logout']);
        Route::get('/user-listing', [AdminController::class, 'userListing']);
    });
});


Route::group([
    'prefix' => 'v1/user',
], function () {

    Route::post('/create', [UserController::class, 'store']);
    Route::post('/login', [UserController::class, 'login']);

    Route::post('/forgot-password', [UserController::class, 'forgotPassword']);
    Route::post('/reset-password-token', [UserController::class, 'resetPasswordToken']);
    
    Route::group(['middleware'=>'authToken'],function(){
        Route::get('/', [UserController::class, 'show']);
        Route::put('/edit', [UserController::class, 'update']);
        Route::delete('/', [UserController::class, 'destroy']);
        Route::get('/logout', [UserController::class, 'logout']);
        Route::post('/orders', [UserController::class, 'getOrders']);

    });

    
});

Route::group(['prefix'=> 'v1/brand'],function()
{
    Route::get('/', [BrandController::class, 'index']);
    Route::get('/{uuid}', [BrandController::class, 'show']);

    Route::group(['middleware' => ['authToken']],function(){
        Route::post('/create', [BrandController::class, 'store']);
        Route::put('/{uuid}', [BrandController::class, 'update']);
        Route::delete('/{uuid}', [BrandController::class, 'destroy']);
    });
});

Route::group(['prefix' => 'v1/category'], function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::get('/{uuid}', [CategoryController::class, 'show']);

    Route::group(['middleware' => ['authToken']], function () {
        Route::post('/create', [CategoryController::class, 'store']);
        Route::put('/{uuid}', [CategoryController::class, 'update']);
        Route::delete('/{uuid}', [CategoryController::class, 'destroy']);
    });
});

Route::group(['prefix' => 'v1/product'], function () {
    Route::get('/', [ProductController::class, 'index']);
    Route::get('/{uuid}', [ProductController::class, 'show']);

    Route::group(['middleware' => ['authToken']], function () {
        Route::post('/create', [ProductController::class, 'store']);
        Route::put('/{uuid}', [ProductController::class, 'update']);
        Route::delete('/{uuid}', [ProductController::class, 'destroy']);
    });
});

Route::group(['prefix'=> 'v1/main'],function(){
    Route::get('blog',[PostController::class,'index']);
    Route::get('blog/{uuid}', [PostController::class, 'show']);
    Route::get('promotions',[PromotionController::class, 'promotion']);
});

Route::group(['prefix' => 'v1/file'], function () {
    Route::post('upload', [FileController::class, 'store'])->middleware('authToken');
    Route::get('{uuid}', [FileController::class, 'show']);
});

Route::get('v1/orders', [OrderController::class, 'index']);
Route::get('v1/orders/shipment-locator', [OrderController::class, 'shipmentLocator']);

Route::group(['prefix' => 'v1/order'], function () {
    Route::post('/create', [OrderController::class, 'store']);
    Route::get('/{uuid}', [OrderController::class, 'show']);
    Route::delete('/{uuid}', [OrderController::class, 'delete']);
});