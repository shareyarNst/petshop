<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::updateOrCreate([
            'title'=> 'Pending Payment',
        ],[
            'uuid' => Str::uuid()
        ]);

        OrderStatus::updateOrCreate([
            'title' => 'Open',
        ], [
            'uuid' => Str::uuid()
        ]);

        OrderStatus::updateOrCreate([
            'title' => 'Paid',
        ], [
            'uuid' => Str::uuid()
        ]);

        OrderStatus::updateOrCreate([
            'title' => 'Shipped',
        ], [
            'uuid' => Str::uuid()
        ]);

        OrderStatus::updateOrCreate([
            'title' => 'Cancelled',
        ], [
            'uuid' => Str::uuid()
        ]);
    }
}
