<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::whereEmail('admin@buckhill.co.uk')->exists();

        if(!$admin){
            User::factory(1)->create([
                'email' => 'admin@buckhill.co.uk',
                'is_admin'=>1,
                'is_marketing'=>1,
                'last_login_at'=> now(),
                'password' => '$2y$10$qBRwwVqoSzRFirbvcqfXuORWzQt7LA/qL1zdSxX0lfmmoM7YOfzYG'
            ]);
        }
    }
}
