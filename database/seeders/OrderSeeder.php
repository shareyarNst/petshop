<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Create Products  **/
        $products = Product::factory(10)->create();

        /** Create Users  **/
        User::factory(10)->create()->each(function ($user) use ($products) {

            /** Create Orders for each user  **/
            Order::factory(5)->create([
                'user_uuid' => $user->uuid,
            ])->each(function ($order) use ($products) {
                /** Assign products, products qty and total for orders  **/
                $orderItems = [];

                $total = 0;
                foreach ($products->random(rand(4, 10)) as $key => $product) {
                    $qty = rand(1, 5);
                    $orderItems[] = [
                        'product'  => $product->uuid,
                        'quantity' => $qty,
                        'price'    => $product->price, // saving price because price can be updated and amount will show wrong 
                        'title'    => $product->title // saving title because title can be updated 
                    ];

                    $total += $qty * $product->price;
                }

                if ($total < env('ORDER_FREE_DELIVERY_AMOUNT', 500)) {
                    $order->delivery_fee = env('DELIVERY_FEE', 15);
                    $total += $order->delivery_fee;
                }

                $order->amount = $total;
                $order->products = $orderItems;
                $order->save();
            });
        });
    }
}
