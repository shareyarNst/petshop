<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->text(20);
        return [
            'uuid' => Str::uuid(),
            'title' => $title,
            'slug' => Str::slug($title,'_'),
            'content' => $this->faker->text(),
            'metadata' => [
                "author" => $this->faker->title(),
                "image" => Str::uuid()
            ],
        ];
    }
}
