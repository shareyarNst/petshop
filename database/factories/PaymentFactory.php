<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = $this->faker->randomElement(['credit_card', 'cash_on_delivery', 'bank_transfer']);
        return [
            'uuid' => Str::uuid(),
            'type' => $type,
            'details' => $this->getPaymentDetails($type)
        ];
    }

    private function getPaymentDetails($type)
    {

        switch ($type) {
            case 'credit_card':
                return  [
                    "holder_name" => $this->faker->name(),
                    "number" => $this->faker->creditCardNumber(),
                    "expire_date" => $this->faker->creditCardExpirationDateString()
                ];
                break;
            case 'cash_on_delivery':
                return [
                    "first_name" => $this->faker->firstName(),
                    "last_name" => $this->faker->lastName(),
                    "address" => $this->faker->address()
                ];
                break;
            case 'bank_transfer':
                return [
                    "iban" => $this->faker->iban(),
                    "name" => $this->faker->name()
                ];
                break;
        }
    }
}
