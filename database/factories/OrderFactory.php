<?php

namespace Database\Factories;

use App\Models\OrderStatus;
use App\Models\Payment;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $orderStatus = OrderStatus::inRandomOrder()->first();
        return [
            'uuid' => Str::uuid(),
            'order_status_uuid' => $orderStatus->uuid,
            'payment_uuid' => Payment::factory(1)->create()->first()->uuid,
            'products' => [],
            'address' => [
                'billing' => $this->faker->address(),
                'shipping' => $this->faker->address(),
            ],
            'amount' => 0,
            'shipped_at' => $orderStatus->title == 'Shipped' ? now():null,
        ];
    }
}
