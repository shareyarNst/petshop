<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PromotionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => Str::uuid(),
            'title' => $this->faker->text(30),
            'content' => $this->faker->text(),
            'metadata' => [
                "valid_from" => date('Y-m-d'),
                "valid_to" => date('Y-m-d'),
                "image" => Str::uuid()
            ],
        ];
    }
}
