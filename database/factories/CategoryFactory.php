<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->text(rand(10, 20));
        return [
            'uuid'  => Str::uuid(),
            'title' => $title,
            'slug'  => Str::slug($title, '_')
        ];
    }
}
