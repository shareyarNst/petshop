<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_uuid' => Str::uuid(),
            'uuid' => Str::uuid(),
            'title' => $this->faker->text(20),
            'description' => $this->faker->text(),
            'price' => rand(10,100),
            'metadata' => [
                'brand' => Str::uuid(),
                'image' => Str::uuid(),
            ],
        ];
    }
}
