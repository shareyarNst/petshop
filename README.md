## About PetShop Setup

After cloning the project follow these steps

- Install dependencies by run `composer install`.
- Run migration `php artisan migrate`.
- Run `php artisan db:seed` for seeding database.
- Create `petshop_testing` for run test.