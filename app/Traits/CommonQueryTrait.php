<?php

namespace App\Traits;

/**
 * A trait to define methods that are common 
 * in multiple models
 */


trait CommonQueryTrait {
    
    /**
     * scope to get record by uuid
     *
     * @param  mixed  $query
     * @param  string $uuid
     * @return mixed
     */
    public function scopeGetByUUID($query,$uuid){

        return $query->where('uuid',$uuid);
    }

    /**
     * scope to get record by title
     *
     * @param  mixed $query
     * @param  string $title
     * @return mixed
     */
    public function scopeGetByTitle($query, string $title)
    {
        return $query->where('title', 'like', '%' . $title . '%');
    }
}
