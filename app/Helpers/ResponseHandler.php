<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;

class ResponseHandler
{

    public static function success(array $body = [], array|string|null $message = null): JsonResponse
    {
        $message = empty($message) ? __('messages.success') : $message;
        return self::send(200, $message, $body);
    }

    public static function validationError(array $validationErrors, array|string|null $message = null): JsonResponse
    {
        $message = empty($message) ? __('messages.validation') : $message;

        return self::send(422, $message, errors: $validationErrors);
    }

    public static function authenticationError(array|string|null $message = null): JsonResponse
    {
        $message = empty($message) ? __('messages.unauthorized') : $message;
        return self::send(401, $message);
    }

    public static function failure(array|string|null $message = null, string $exception = null): JsonResponse
    {
        $message = empty($message) ? __('messages.failed') : $message;
        return self::send(400, $message, exception: $exception);
    }

    public static function noRecordFound(array|string|null $message = null, string $attr = 'uuid'): JsonResponse
    {
        $message = empty($message) ? __('messages.no_record', ['attribute' => $attr]) : $message;
        return self::send(404, $message);
    }

    private static function send(int $status, array|string|null $message, array $body = [], array $errors = [], string $exception = null): JsonResponse
    {

        $response =  [
            'status'        =>  $status,
            'message'       =>  $message,
            'data'          =>  empty($body) ? (object)$body : $body,
            'errors'        =>  $errors,
            'exception'     =>  $exception,
        ];

        return response()->json($response, $status, [], JSON_UNESCAPED_UNICODE);
    }
}
