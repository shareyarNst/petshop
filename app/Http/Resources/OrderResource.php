<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'uuid'          => $this->uuid,
            'products'      => $this->products,
            'address'       => $this->address,
            'subtotal'      => $this->subtotal(),
            'delivery_fee'  => number_format($this->delivery_fee,2),
            'total'         => number_format($this->amount,2),
            'user'          => new UserResource($this->user),
            'payment'       => new PaymentResource($this->payment),
            'order_status'  => $this->orderStatus->title,
            'shipped_at'    => $this->shipped_at
        ];
    }
}
