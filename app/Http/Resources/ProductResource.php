<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'uuid'          => $this->uuid,
            'category_uuid' => $this->category_uuid,
            'title'         => $this->title,
            'description'   => $this->description,
            'price'         => (float)number_format($this->price,2),
            'metadata'      => $this->metadata,
        ];
    }
}
