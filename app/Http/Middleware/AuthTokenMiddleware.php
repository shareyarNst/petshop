<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Service\JWTService;
use App\Models\JWTToken;
use Illuminate\Support\Facades\Auth;

class AuthTokenMiddleware
{
    /**
     * Handle an incoming request to check whether the request has valid token or not.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        $validateToken = [];

        if($token){
            $validateToken = (new JWTService)->validateToken($token);
            
            /** if token status is not validated return back with error message */
            if(!$validateToken['status']){
                return ResponseHandler::authenticationError(message: $validateToken['message']);
            }
            /** if token does not exists return back with error message */
            else if (!JWTToken::getTokenByUniqueId($validateToken['token']->get('unique_id'))->exists()){
                return ResponseHandler::authenticationError(__('messages.token.expired'));
            }
        }
        else{
            /** if token is not present return back with error message */
            return ResponseHandler::authenticationError(message:__('messages.token.missing'));
        }

        /** get user by user_uuid */
        $user = User::where('uuid', $validateToken['token']->get('user_uuid'))->first();
        
        if(!$user){
            return ResponseHandler::authenticationError();
        }

        /** Login user using id */
        Auth::loginUsingId($user->id);
        
        return $next($request);
    }
}
