<?php

namespace App\Http\Controllers\Api;

use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\PromotionResource;

class PromotionController extends Controller
{
    /**
     * @OA\GET(
     *      path="/api/v1/main/promotions",
     *      operationId="promotion",
     *      tags={"Main Page"},
     *      summary="Return list of promotions",
     *      
     *      @OA\Parameter(
     *          name="title",
     *          description="Title",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="title"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function promotion(Request $request)
    {
        try {
            $promotions = Promotion::getPromotionsListing($request);
            
            $response = PromotionResource::collection($promotions)->response()->getData(true);

            $result['promotions'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
