<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Models\User;
use App\Models\JWTToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Service\JWTService;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseApiController;

class AdminController extends BaseApiController
{
    /**
     * @OA\POST(
     *      path="/api/v1/admin/login",
     *      operationId="login",
     *      tags={"Admin"},
     *      summary="Login Admin Account",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"email","password"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity"
     *          )
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal Server Error"
     *          )
     *      )
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {

            $inputs = $request->only('email', 'password');

            $validator = Validator::make($inputs, User::loginRules());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            if (!Auth::attempt($inputs)) {
                return ResponseHandler::authenticationError(__('messages.auth_failed'));
            }

            $user = Auth::user();

            if ($user && !$user->is_admin) {
                Auth::logout();
                return ResponseHandler::authenticationError();
            }

            /** update user last login **/
            $user->last_login_at = now();
            $user->Save();
            /** update user last login **/

            // JWTService to create token 
            $jwtService = new JWTService();
            $token = $jwtService->createToken();

            // saving token details
            $jwtToken = JWTToken::createUserToken($user->id, $token);

            return ResponseHandler::success(['token' => $token->toString()]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/admin/logout",
     *      operationId="logout",
     *      tags={"Admin"},
     *      summary="Logout Admin User",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * logout
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {

            $token = (string)$request->bearerToken();

            $token = (new JWTService)->getParsedToken($token);

            $deleteToken = JWTToken::getTokenByUniqueId($token->claims()->get('unique_id'))->delete();

            return ResponseHandler::success(message:__('messages.logout'));

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }

    /**
     * @OA\POST(
     *      path="/api/v1/admin/create",
     *      operationId="store",
     *      tags={"Admin"},
     *      summary="Create Admin Account",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 required={"first_name","last_name","email","password","password_confirmation",
     *                 "avatar","address","phone_number"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="first_name",
     *                     description="First Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     description="Last Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Password Confirmation",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     description="Address",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="avatar",
     *                     description="Avatar",
     *                     type="file",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Phone Number",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="is_marketing",
     *                     description="Marketing",
     *                     type="integer",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            $inputs = $request->only([
                'first_name', 'last_name', 'email', 'password',
                'password_confirmation', 'avatar', 'address', 'phone_number'
            ]);

            $validator = Validator::make($inputs, User::storeUserRules());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            if (isset($request->avatar)) {
                $avatarUpload = File::createFile($request->avatar);

                if (!$avatarUpload) {
                    return ResponseHandler::failure(message: "File upload failed");
                }

                $inputs['avatar'] = $avatarUpload->uuid;
            }
            
            $inputs['uuid'] = Str::uuid();
            $inputs['is_admin'] = true;
            $adminUser = User::create($inputs);

            return ResponseHandler::success(['user' => $adminUser]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/admin/user-listing",
     *      operationId="userListing",
     *      tags={"Admin"},
     *      summary="Get Users Listing",
     *      security={{"bearerAuth":{}}},
     *      
     *      @OA\Parameter(
     *          name="first_name",
     *          description="Name",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          description="Email",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="phone_number",
     *          description="Phone Number",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="created_at",
     *          description="Create Date",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_marketing",
     *          description="Marketing",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     * 
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     * ),
     */

    /**
     * userListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userListing(Request $request)
    {
        try {

            $users = User::getUserListing($request);

            $response = UserResource::collection($users)->response()->getData(true);

            $result['users'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
