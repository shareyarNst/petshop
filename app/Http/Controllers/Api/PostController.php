<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;

class PostController extends Controller
{
    /**
     * @OA\GET(
     *      path="/api/v1/main/blog",
     *      operationId="index",
     *      tags={"Main Page"},
     *      summary="Return list of blogs",
     *      
     *      @OA\Parameter(
     *          name="title",
     *          description="Title",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="author",
     *          description="Author",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="title"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $posts = Post::getPostsListing($request);

            $response = PostResource::collection($posts)->response()->getData(true);

            $result['posts'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/main/blog/{uuid}",
     *      operationId="show",
     *      tags={"Main Page"},
     *      summary="Return single blog",
     *      
     *      @OA\Parameter(
     *          name="uuid",
     *          description="Product UUID",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        try {
            $post = Post::getByUUID($uuid)->first();
            return ResponseHandler::success(['blog' => new PostResource($post)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

}
