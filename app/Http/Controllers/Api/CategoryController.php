<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * @OA\GET(
     *      path="/api/v1/category",
     *      operationId="index",
     *      tags={"Category"},
     *      summary="Return all categories",
     * 
     *      
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * index
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {

            $categories = Category::query()
                ->when(isset($request->sortBy) && in_array($request->sortBy, ['title']), function ($query) use ($request) {
                    $query->orderBy($request->sortBy, $request->desc == 'true' ? 'desc' : 'asc');
                })
                ->paginate($request->limit ?? 10, ['*'], 'page', request()->page);


            $response = CategoryResource::collection($categories)->response()->getData(true);

            $result['categories'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\POST(
     *      path="/api/v1/category/create",
     *      operationId="store",
     *      tags={"Category"},
     *      summary="Create Category",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"title"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="title",
     *                     description="Title",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            $inputs = $request->only(['title']);

            $validator = Validator::make($inputs, ['title' => 'required|unique:brands,title']);

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $inputs['uuid'] = Str::uuid();
            $inputs['slug'] = Str::slug($inputs['title'], '_');
            $newBrand = Category::create($inputs);

            return ResponseHandler::success(['category' => new CategoryResource($newBrand)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }


    /**
     * @OA\PUT(
     *      path="/api/v1/category/{uuid}",
     *      operationId="update",
     *      tags={"Category"},
     *      summary="Update Category",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"title"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="title",
     *                     description="Title",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     *     
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $uuid)
    {
        try {

            $category = Category::getByUUID($uuid)->first();

            if (!$category) {
                return ResponseHandler::noRecordFound();
            }

            $inputs = $request->only(['title']);

            $validator = Validator::make($inputs, ['title' => 'required|unique:brands,title']);

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $inputs['slug'] = Str::slug($inputs['title'], '_');
            $updateCategory = $category->updateOrCreate(['uuid' => $category->uuid], $inputs);

            return ResponseHandler::success(['user' => new CategoryResource($updateCategory)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/category/{uuid}",
     *      operationId="show",
     *      tags={"Category"},
     *      summary="Get Single Category",
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * show
     *
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        try {

            $category = Category::getByUUID($uuid)->first();

            if (!$category) {
                return ResponseHandler::noRecordFound();
            }

            return ResponseHandler::success(['category' => new CategoryResource($category)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\DELETE(
     *      path="/api/v1/category/{uuid}",
     *      operationId="destroy",
     *      tags={"Category"},
     *      summary="Delete Category",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        try {

            $category = Category::getByUUID($uuid)->first();

            if (!$category) {
                return ResponseHandler::noRecordFound();
            }

            $category->delete();
            return ResponseHandler::success(message: __('messages.delete'));
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
