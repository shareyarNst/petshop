<?php

namespace App\Http\Controllers\Api;

use Storage;
use Validator;
use App\Models\File;
use App\Helpers\Helpers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\FileResource;

class FileController extends Controller
{

    /**
     * @OA\POST(
     *      path="/api/v1/file/upload",
     *      operationId="login",
     *      tags={"File"},
     *      summary="Upload file",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 required={"file"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="file",
     *                     description="File",
     *                     type="file",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            
            $input = $request->only('file');

            $validator = Validator::make($input,['file'=>'required|image|mimes:jpg,png,jpeg|max:1024']);

            if($validator->fails()){
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $file = File::createFile($request->file);

            if(!$file){
                return ResponseHandler::failure(message: 'File upload failed.');
            }

            return ResponseHandler::success(['file' => new FileResource($file)]);
            
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/file/{uuid}",
     *      operationId="show",
     *      tags={"File"},
     *      summary="Get Single file",
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        try {

            $file = File::getByUUID($uuid)->first();

            if(!$file){
                return ResponseHandler::noRecordFound();
            }

            return ResponseHandler::success(['file'=> new FileResource($file)]);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
