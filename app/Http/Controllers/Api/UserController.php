<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Models\User;
use App\Models\Order;
use App\Models\JWTToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Helpers\ResponseHandler;
use App\Http\Service\JWTService;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\OrderResource;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @OA\POST(
     *      path="/api/v1/user/login",
     *      operationId="login",
     *      tags={"User"},
     *      summary="Login User Account",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"email","password"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {

            $inputs = $request->only('email', 'password');

            $validator = Validator::make($inputs, User::loginRules());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            if (!Auth::attempt($inputs)) {
                return ResponseHandler::authenticationError();
            }

            $user = Auth::user();

            if ($user && $user->is_admin) {
                return ResponseHandler::authenticationError();
            }

            /** update user last login **/
            $user->last_login_at = now();
            $user->save();
            /** update user last login **/

            // JWTService to create token 
            $jwtService = new JWTService();
            $token = $jwtService->createToken();

            // saving token details
            $jwtToken = JWTToken::createUserToken($user->id, $token);

            return ResponseHandler::success(['token' => $token->toString()]);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/user/logout",
     *      operationId="logout",
     *      tags={"User"},
     *      summary="Logout User",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * logout
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {

            $token = (string)$request->bearerToken();

            $token = (new JWTService)->getParsedToken($token);

            $deleteToken = JWTToken::getTokenByUniqueId($token->claims()->get('unique_id'))->delete();

            return ResponseHandler::success(message: __('messages.logout'));
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }


    /**
     * @OA\POST(
     *      path="/api/v1/user/create",
     *      operationId="store",
     *      tags={"User"},
     *      summary="Create User Account",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 required={"first_name","last_name","email","password","password_confirmation",
     *                 "avatar","address","phone_number"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="first_name",
     *                     description="First Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     description="Last Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Password Confirmation",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     description="Address",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="avatar",
     *                     description="Avatar",
     *                     type="file",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Phone Number",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="is_marketing",
     *                     description="Marketing",
     *                     type="integer",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            $inputs = $request->only([
                'first_name', 'last_name', 'email', 'password',
                'password_confirmation', 'avatar', 'address', 'phone_number', 'is_marketing'
            ]);

            $validator = Validator::make($inputs, User::storeUserRules());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            if (isset($request->avatar)) {
                $avatarUpload = File::createFile($request->avatar);

                if (!$avatarUpload) {
                    return ResponseHandler::failure(message: "File upload failed");
                }

                $inputs['avatar'] = $avatarUpload->uuid;
            }

            $inputs['uuid'] = Str::uuid();
            $newUser = User::create($inputs);

            return ResponseHandler::success(['user' => new UserResource($newUser)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/user",
     *      operationId="show",
     *      tags={"User"},
     *      summary="Return User Detail",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * show
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        try {
            return ResponseHandler::success(['user' => new UserResource(Auth::user())]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\PUT(
     *      path="/api/v1/user/edit",
     *      operationId="update",
     *      tags={"User"},
     *      summary="Update User Account",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"first_name","last_name","email","address","phone_number"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="first_name",
     *                     description="First Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     description="Last Name",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Password Confirmation",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     description="Address",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="avatar",
     *                     description="Avatar",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Phone Number",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="is_marketing",
     *                     description="Marketing",
     *                     type="integer",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * update
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        try {

            $inputs = $request->only([
                'first_name', 'last_name', 'email', 'password',
                'password_confirmation', 'avatar', 'address', 'phone_number', 'is_marketing'
            ]);

            $validator = Validator::make($inputs, User::storeUserRules(false));

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            if (isset($inputs['password'])) {
                $inputs['password'] = bcrypt($inputs['password']);
                unset($inputs['password_confirmation']);
            }

            $updateUser = User::where('id', Auth::id())->update($inputs);
            $user = User::find(Auth::id());

            return ResponseHandler::success(['user' => new UserResource($user)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\DELETE(
     *      path="/api/v1/user",
     *      operationId="destroy",
     *      tags={"User"},
     *      summary="Delete User Account",
     *      security={{"bearerAuth":{}}},
     *      
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * destroy
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        try {

            $user = Auth::user();

            if(!$user){
                return ResponseHandler::noRecordFound();
            }

            $user->tokens()->delete();

            $user->delete();

            return ResponseHandler::success(message: __('messages.delete'));
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\POST(
     *      path="/api/v1/user/forgot-password",
     *      operationId="forgotPassword",
     *      tags={"User"},
     *      summary="Forgot User password",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"email"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * forgotPassword
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request)
    {
        try {

            $inputs = $request->only('email');
            $validator = Validator::make($inputs, User::forgotPasswordRule());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $user = User::getUserByEmail($inputs['email'])->first();

            if (!$user) {
                return ResponseHandler::failure(message: __('password.user'));
            }

            $createResetToken = PasswordReset::updateOrCreate([
                'email'      => $user->email,
            ], [
                'token'      => Str::random(20),
                'created_at' => now()
            ]);


            return ResponseHandler::success(['reset-token' => $createResetToken->token]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }


    /**
     * @OA\POST(
     *      path="/api/v1/user/reset-password-token",
     *      operationId="resetPasswordToken",
     *      tags={"User"},
     *      summary="Reset password token",
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"token","password","password_confirmation"},
     *                 type="object",
     *                 
     *                 @OA\Property(
     *                     property="token",
     *                     description="Token",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Password",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Password Confirmation",
     *                     type="string",
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * resetPasswordToken
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPasswordToken(Request $request)
    {
        try {

            $inputs = $request->only('token','password','password_confirmation');
            $validator = Validator::make($inputs, User::resetPasswordTokenRule());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $resetToken = PasswordReset::getByToken($inputs['token'])->first();

            if (!$resetToken) {
                return ResponseHandler::failure(message:__('passwords.token'));
            }

            $user = User::getUserByEmail($resetToken->email)->first();

            if (!$user) {
                return ResponseHandler::failure(message: __('passwords.user'));
            }

            $updateUserPassword = $user->update(['password'=> bcrypt($inputs['password'])]);

            $resetToken->delete();

            return ResponseHandler::success();
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/user/orders",
     *      operationId="getOrders",
     *      tags={"User"},
     *      summary="Get User Orders Listing",
     *      security={{"bearerAuth":{}}},
     *      
     *      @OA\Parameter(
     *          name="date_range",
     *          description="date_range",
     *          in="query",
     *          @OA\Schema(
     *              type="object",
     *              @OA\Property(
     *                property="from",
     *                type="string",
     *                example="2022-02-05"
     *              ),
     *              @OA\Property(
     *                property="to",
     *                type="string",
     *                example="2022-03-05"
     *              ),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="created_at,price"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     * ),
     */

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders(Request $request)
    {
        try {

            $orders = Order::getUserOrderListing($request,Auth::user()->uuid);

            $response = OrderResource::collection($orders)->response()->getData(true);

            $result['orders'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
