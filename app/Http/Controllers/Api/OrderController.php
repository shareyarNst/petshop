<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHandler;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * @OA\GET(
     *      path="/api/v1/orders",
     *      operationId="index",
     *      tags={"Order"},
     *      summary="Get Orders Listing",
     *      
     *      @OA\Parameter(
     *          name="date_range",
     *          description="date_range",
     *          in="query",
     *          @OA\Schema(
     *              type="object",
     *              @OA\Property(
     *                property="from",
     *                type="string",
     *                example="2022-02-05"
     *              ),
     *              @OA\Property(
     *                property="to",
     *                type="string",
     *                example="2022-03-05"
     *              ),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="created_at,price"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     * ),
     */

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {

            $orders = Order::getOrderListing($request);

            $response = OrderResource::collection($orders)->response()->getData(true);

            $result['orders'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/orders/shipment-locator",
     *      operationId="shipmentLocator",
     *      tags={"Order"},
     *      summary="Get Shipped Orders Listing",
     *      
     *      @OA\Parameter(
     *          name="date_range",
     *          description="date_range",
     *          in="query",
     *          @OA\Schema(
     *              type="object",
     *              @OA\Property(
     *                property="from",
     *                type="string",
     *                example="2022-02-05"
     *              ),
     *              @OA\Property(
     *                property="to",
     *                type="string",
     *                example="2022-03-05"
     *              ),
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="customer",
     *          description="customer",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="John Doe"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="uuid",
     *          description="Order UUID",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="dcaf35b8-c85b-4a34-beed-b82762bf2946"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              example="created_at"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     * ),
     */

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function shipmentLocator(Request $request)
    {
        try {

            $orders = Order::getShippedOrderListing($request);

            $response = OrderResource::collection($orders)->response()->getData(true);

            $result['orders'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }


    /**
     * @OA\GET(
     *      path="/api/v1/order/{uuid}",
     *      operationId="show",
     *      tags={"Order"},
     *      summary="Get Single Order",
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        try {
            
            $order = Order::getByUUID($uuid)->first();
            return ResponseHandler::success(['order'=>new OrderResource($order)]);

        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }


    /**
     * @OA\DELETE(
     *      path="/api/v1/order/{uuid}",
     *      operationId="show",
     *      tags={"Order"},
     *      summary="Delete Single Order",
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        try {

            $order = Order::getByUUID($uuid)->first();

            if (!$order) {
                return ResponseHandler::noRecordFound();
            }

            $order->delete();
            return ResponseHandler::success(message: __('messages.delete'));
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception:$e->getMessage());
        }
    }
}
