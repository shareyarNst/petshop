<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * @OA\GET(
     *      path="/api/v1/product",
     *      operationId="index",
     *      tags={"Product"},
     *      summary="Return all products",
     *      
     *      @OA\Parameter(
     *          name="title",
     *          description="Title",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="price",
     *          description="Price",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="category_uuid",
     *          description="Category UUID",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="brand",
     *          description="Brand",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sortBy",
     *          description="sortBy",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="desc",
     *          description="desc",
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="limit",
     *          description="limit",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * index
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {

            $products = Product::getProductsListing($request);

            $response = ProductResource::collection($products)->response()->getData(true);

            $result['products'] = $response['data'];

            $result = array_merge($result, $response['meta']);

            return ResponseHandler::success($result);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\POST(
     *      path="/api/v1/product/create",
     *      operationId="store",
     *      tags={"Product"},
     *      summary="Create Product",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"category_uuid","title","description","price","metadata"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="category_uuid",
     *                     description="Category UUID",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     description="Title",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     description="Price",
     *                     type="number",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="metadata",
     *                     description="Metadata",
     *                     type="object",
     *                     
     *                     @OA\Property(
     *                          property="image",
     *                          type="string",
     *                      ),
     *                     @OA\Property(
     *                          property="brand",
     *                          type="string",
     *                      ),
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {

            $inputs = $request->only('category_uuid', 'title', 'price', 'description', 'metadata');

            $validator = Validator::make($inputs, Product::storeProductRule());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $metadata = json_decode($inputs['metadata'],true);

            $metadataValidator = Validator::make($metadata, Product::metadataRule(),Product::metadataRuleMessages());

            if ($metadataValidator->fails()) {
                return ResponseHandler::validationError($metadataValidator->errors()->all());
            }

            $inputs['uuid'] = Str::uuid();
            $inputs['metadata'] = $metadata;
            $product = Product::create($inputs);

            return ResponseHandler::success(['product' => new ProductResource($product)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\PUT(
     *      path="/api/v1/product/{uuid}",
     *      operationId="update",
     *      tags={"Product"},
     *      summary="Update Product",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *      ),
     * 
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 required={"category_uuid","title","description","price","metadata"},
     *                 type="object",
     *                 @OA\Property(
     *                     property="category_uuid",
     *                     description="Category UUID",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     description="Title",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description",
     *                     type="string",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     description="Price",
     *                     type="number",
     *                     
     *                 ),
     *                 @OA\Property(
     *                     property="metadata",
     *                     description="Metadata",
     *                     type="object",
     *                     
     *                     @OA\Property(
     *                          property="image",
     *                          type="string",
     *                      ),
     *                     @OA\Property(
     *                          property="brand",
     *                          type="string",
     *                      ),
     *                     
     *                 ),
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */
    /**
     * Update a existing resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $uuid)
    {
        try {

            $product = Product::getByUUID($uuid)->first();

            if (!$product) {
                return ResponseHandler::noRecordFound();
            }

            $inputs = $request->only('category_uuid', 'title', 'price', 'description', 'metadata');

            $validator = Validator::make($inputs, Product::storeProductRule());

            if ($validator->fails()) {
                return ResponseHandler::validationError($validator->errors()->all());
            }

            $metadata = json_decode($inputs['metadata'], true);

            $metadataValidator = Validator::make($metadata, Product::metadataRule(), Product::metadataRuleMessages());

            if ($metadataValidator->fails()) {
                return ResponseHandler::validationError($metadataValidator->errors()->all());
            }
            
            $inputs['metadata'] = $metadata;
            $updateProduct = $product->updateOrCreate(['uuid' => $product->uuid], $inputs);

            return ResponseHandler::success(['product' => new ProductResource($updateProduct)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\GET(
     *      path="/api/v1/product/{uuid}",
     *      operationId="show",
     *      tags={"Product"},
     *      summary="Get Single product",
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * show
     *
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        try {

            $product = Product::getByUUID($uuid)->first();

            if (!$product) {
                return ResponseHandler::noRecordFound();
            }

            return ResponseHandler::success(['product' => new ProductResource($product)]);
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }

    /**
     * @OA\DELETE(
     *      path="/api/v1/product/{uuid}",
     *      operationId="destroy",
     *      tags={"Product"},
     *      summary="Delete product",
     *      security={{"bearerAuth":{}}},
     * 
     *      @OA\Parameter(
     *         name="uuid",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * 
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *          ),
     *      @OA\Response(
     *          response=404,
     *          description="Page Not Found"
     *          )
     *      ),
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        try {

            $product = Product::getByUUID($uuid)->first();

            if (!$product) {
                return ResponseHandler::noRecordFound();
            }

            $product->delete();
            return ResponseHandler::success(message: __('messages.delete'));
        } catch (\Exception $e) {
            return ResponseHandler::failure(exception: $e->getMessage());
        }
    }
}
