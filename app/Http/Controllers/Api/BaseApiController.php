<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class BaseApiController extends Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Pet Shop Api Documentation",
     *      description="Pet Shop Api Documentation"
     * )
     *
     */

    /**
     * @OA\Tag(
     *     name="Admin",
     *     description="API Endpoints of Admin"
     * )
     */

    /**
     * @OA\Tag(
     *     name="User",
     *     description="API Endpoints of User"
     * )
     */

    /**
     * @OA\Tag(
     *     name="Brand",
     *     description="API Endpoints of Brand"
     * )
     */

    /**
     * @OA\Tag(
     *     name="Category",
     *     description="API Endpoints of Category"
     * )
     */

    /**
     * @OA\Tag(
     *     name="Product",
     *     description="API Endpoints of Product"
     * )
     */

    /**
     * @OA\Tag(
     *     name="Main Page",
     *     description="API Endpoints of Main Page"
     * )
     */

    /**
     * @OA\SecurityScheme(
     *     securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * )
     */
}
