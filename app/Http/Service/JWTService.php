<?php

namespace App\Http\Service;

use DateTimeImmutable;
use Lcobucci\JWT\Signer;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\PermittedFor;

class JWTService
{

    private Configuration $configuration;

    public function __construct()
    {
        $this->configuration = Configuration::forAsymmetricSigner(
            new Signer\Rsa\Sha256(),
            InMemory::file('C:\Users\Hp\Downloads\pgtl.pem'),
            InMemory::file('C:\Users\Hp\Downloads\pgtl.ppk')
        );
    }

    /**
     * function to create token for auth user
     *
     * @return mixed
     */
    public function createToken()
    {
        $config = $this->configuration;
        $now = new DateTimeImmutable();
        $token = $config->builder()
            ->issuedBy(env('APP_DOMAIN'))
            ->permittedFor(env('APP_DOMAIN'))
            // ->identifiedBy('4f1g23a12aa')
            ->issuedAt($now)
            ->expiresAt($now->modify("+" . env('API_TOKEN_EXPIRE', '1') . " hour"))
            ->withClaim('user_uuid', Auth::user()->uuid)
            ->withClaim('unique_id', Str::random(6))
            ->getToken($config->signer(), $config->signingKey());

        return $token;
    }

    
    /**
     * validateToken
     *
     * @param  string $token
     * @return mixed
     */
    public function validateToken(string $token)
    {
        $validated = ['status' => false, 'message' => 'Token is valid','token' => []];
        $this->configuration->setValidationConstraints(
            new PermittedFor(env('APP_DOMAIN')),
            new IssuedBy(env('APP_DOMAIN')),
            // new SignedWith($config->signer(), $config->signingKey())
        );

        $config = $this->configuration;

        $token = $config->parser()->parse($token);

        $constraints = $config->validationConstraints();

        if (!$config->validator()->validate($token, ...$constraints)) {
            $validated['message'] = __('messages.token.invalid');
            return $validated;
        }

        if ($token->isExpired(now())) {
            $validated['message'] = __('messages.token.expired');
            return $validated;
        }

        $validated['status'] = true;
        $validated['token']  = $token->claims();
        return $validated;
    }
    
    /**
     * getParsedToken
     *
     * @param  string $token
     * @return mixed
     */
    public function getParsedToken(string $token)
    {
        $config = $this->configuration;

        $token = $config->parser()->parse($token);

        return $token;
    }
}
