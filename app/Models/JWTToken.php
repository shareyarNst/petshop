<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JWTToken extends Model
{
    protected $table = 'jwt_tokens';

    protected $fillable = ['user_id','unique_id','token_title','last_used_at','expires_at'];

    
    /**** scopes ****/

        
    /**
     * scope to check token using unique id
     *
     * @param  mixed $query
     * @param  mixed $uniqueId
     * @return mixed
     */
    public function scopeGetTokenByUniqueId($query,$uniqueId)
    {
        return $query->where('unique_id',$uniqueId);
    }


    /**** scopes ****/

    /**
     * createUserToken
     *
     * @param  int   $userId
     * @param  mixed $authToken
     * @return \App\Models\JWTToken
     */
    public static function createUserToken(int $userId,$authToken): JWTToken
    {
        $token = self::create([
            'user_id'     => $userId,
            'unique_id'   => $authToken->claims()->get('unique_id'),
            'token_title' => 'Auth-Token',
            'last_used_at'=> $authToken->claims()->get('iat'),
            'expires_at'  => $authToken->claims()->get('exp')
        ]);

        return $token;
    }
}
