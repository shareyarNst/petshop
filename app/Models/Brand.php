<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid','title','slug'];

    /**** scopes ****/

    /**** scopes ****/
}
