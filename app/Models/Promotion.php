<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Promotion extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid','title','content','metadata'];

    protected $casts = [
        'metadata' => 'array'
    ];

    /**** scopes ****/

    /**** scopes ****/

    /**
     * getProductListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getPromotionsListing(Request $request)
    {
        $products = self::when(isset($request->title), function ($query) use ($request) {
                $query->getByTitle($request->title);
            })
            ->when(isset($request->sortBy) && in_array($request->sortBy, ['title']), function ($query) use ($request) {
                $query->orderBy($request->sortBy, $request->desc == 'true' ? 'desc' : 'asc');
            })
            ->paginate($request->limit ?? 20);

        return $products;
    }
}
