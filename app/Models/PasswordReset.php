<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'email';
    protected $fillable = ['email','token','created_at'];

    /**** scopes ****/
    
    /**
     * scopeGetEmailByToken
     *
     * @param  mixed $query
     * @param  mixed $token
     * @return mixed
     */
    public function scopeGetByToken($query,$token)
    {
        return $query->where('token',$token);
    }
    
    /**** scopes ****/
}
