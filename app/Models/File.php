<?php

namespace App\Models;

use Request;
use App\Helpers\Helpers;
use Illuminate\Support\Str;
use App\Traits\CommonQueryTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class File extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid', 'name', 'path', 'size', 'type'];


    /**
     * formatBytes
     *
     * @param  int $bytes
     * @param  int $precision
     * @return string
     */
    public static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
    
    /**
     * createFile
     *
     * @param  \Illuminate\Http\UploadedFile $image
     * @return mixed
     */
    public static function createFile($image)
    {
        $imageName   = time() . '.' . $image->getClientOriginalExtension();
        $uploadedImg = Storage::disk('public')->putFileAs(env('IMAGE_DIR', 'pet-shop'), $image, $imageName);;

        if (!$uploadedImg) {
            return false;
        }

        $file = self::create(array(
            'uuid'  => Str::uuid(),
            'name'  => $imageName,
            'path'  => $uploadedImg,
            'size'  => $image->getSize(),
            'type'  => $image->getMimeType()
        ));

        return $file;
    }
}
