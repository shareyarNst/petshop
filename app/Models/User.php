<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'uuid',
        'first_name',
        'last_name',
        'email',
        'password',
        'avatar',
        'address',
        'phone_number',
        'is_admin',
        'is_marketing',
        'last_login_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * loginRules
     *
     * @return array
     */
    public static function loginRules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    /**
     * storeUserRules
     *
     * @param  bool $create
     * @return array
     */
    public static function storeUserRules(bool $create = true): array
    {
        return [
            'first_name'   => 'required|string',
            'last_name'    => 'required|string',
            'email'        => 'required|email',
            'password'     => $create ? 'required|confirmed' : 'confirmed',
            'avatar'       => $create ? 'required|image|mimes:jpg,png,jpeg|max:1024' 
                                                    : 'string|exists:files,uuid',
            'address'      => 'required|string',
            'phone_number' => 'required|string',
            'is_marketing' => 'integer'
        ];
    }

    /**
     * forgotPasswordRule
     *
     * @return array
     */
    public static function forgotPasswordRule(): array
    {
        return [
            'email' => 'required|email'
        ];
    }

    /**
     * resetPasswordRule
     *
     * @return array
     */
    public static function resetPasswordTokenRule(): array
    {
        return [
            'token'    => 'required',
            'password' => 'required|confirmed'
        ];
    }

    /**** scopes ****/

    /**
     * scope function to apply admin check
     *
     * @param  mixed $query
     * @param  bool $admin
     * @return mixed
     */
    public function scopeAdmin($query, bool $admin = true)
    {
        return $query->where('is_admin', $admin);
    }

    /**
     * scopeGetUserByEmail
     *
     * @param  mixed $query
     * @param  string $email
     * @return mixed
     */
    public function scopeGetUserByEmail($query, string $email)
    {
        return $query->where('email', $email);
    }

    /**** scopes ****/


    /**** relations ****/

    /**
     * relation to return user token
     *
     * @return HasOne
     */
    public function token(): HasOne
    {
        return $this->hasOne(JWTToken::class);
    }
    
    /**
     * tokens
     *
     * @return HasMany
     */
    public function tokens(): HasMany
    {
        return $this->hasMany(JWTToken::class);
    }
    
    /**
     * orders
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class,'user_uuid','uuid');
    }

    /**** relations ****/
    
    /**
     * getUserListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getUserListing(Request $request)
    {
        $users = self::when(isset($request->first_name), function ($query) use ($request) {

            return $query->where('first_name', 'like', '%' . $request->first_name . '%')
                ->orWhere('last_name', 'like', '%' . $request->first_name . '%');
        })->when(isset($request->phone_number), function ($query) use ($request) {

            return $query->where('phone_number', $request->phone_number);
        })->when(isset($request->email), function ($query) use ($request) {

            return $query->where('email', $request->email);
        })->when(isset($request->created_at), function ($query) use ($request) {

            return $query->where('created_at', '>=', $request->created_at);
        })->when(isset($request->is_marketing), function ($query) use ($request) {

            return $query->where('is_marketing', $request->is_marketing);
        })
            ->admin(false)
            ->paginate($request->limit ?? 10, ['*'], 'page', request()->page);

        return $users;
    }
}
