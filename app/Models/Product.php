<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;
    use CommonQueryTrait;

    protected $fillable = ['uuid', 'category_uuid', 'title', 'description', 'price', 'metadata'];

    protected $casts = [
        'metadata' => 'array'
    ];
    /**
     * storeProductRule
     *
     * @return array
     */
    public static function storeProductRule(): array
    {
        return [
            'category_uuid' => 'required|string|exists:categories,uuid',
            'title'         => 'required|string',
            'description'   => 'required|string',
            'price'         => 'required|numeric',
            'metadata'      => 'required|json',
        ];
    }

    /**
     * metadataRule
     *
     * @return array
     */
    public static function metadataRule(): array
    {
        return [
            'brand'       => 'required|string|exists:brands,uuid',
            'image'       => 'required|string|exists:files,uuid',
        ];
    }

    /**
     * metadataRuleMessages
     *
     * @return array
     */
    public static function metadataRuleMessages(): array
    {
        return [
            'brand.required' => 'The metadata brand field is required.',
            'image.required' => 'The metadata image field is required.',
        ];
    }

    /**** scopes ****/

    /**
     * scopeGetByCategory
     *
     * @param  mixed $query
     * @param  string $categoryUUID
     * @return mixed
     */
    public function scopeGetByCategory($query, string $categoryUUID)
    {
        return $query->where('category_uuid', $categoryUUID);
    }

    /**
     * scopeGetByPrice
     *
     * @param  mixed $query
     * @param  float|int $price
     * @return mixed
     */
    public function scopeGetByPrice($query, float|int $price)
    {
        return $query->where('price', '>=', $price);
    }

    /**
     * scopeGetByBrand
     *
     * @param  mixed $query
     * @param  string $brandUUID
     * @return mixed
     */
    public function scopeGetByBrand($query, string $brandUUID)
    {
        return $query->where('metadata', 'like', '%"brand":"' . $brandUUID . '"%');
    }

    /**** scopes ****/

    /**** relations ****/

    /**
     * category
     *
     * @return BelongsTo
     */
    protected function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'uuid', 'category_uuid');
    }
    /**** relations ****/


    /**
     * getProductListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getProductsListing(Request $request)
    {
        $products = self::when(isset($request->category_uuid), function ($query) use ($request) {
            $query->getByCategory($request->category_uuid);
        })
            ->when(isset($request->title), function ($query) use ($request) {
                $query->getByTitle($request->title);
            })
            ->when(isset($request->price), function ($query) use ($request) {
                $query->getByPrice($request->price);
            })
            ->when(isset($request->brand), function ($query) use ($request) {
                $query->getByBrand($request->brand);
            })
            ->when(isset($request->sortBy) && in_array($request->sortBy, ['title', 'price']), function ($query) use ($request) {
                $query->orderBy($request->sortBy, $request->desc == 'true' ? 'desc' : 'asc');
            })
            ->paginate($request->limit ?? 20, ['*'], $request->page);

        return $products;
    }
}
