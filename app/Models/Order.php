<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid', 'user_uuid', 'order_status_uuid', 'payment_uuid', 'products', 'address', 'delivery_fee', 'amount', 'shipped_at'];

    protected $casts = [
        'products' => 'array',
        'address' => 'array',
    ];

    /** relations **/

    /**
     * user
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_uuid', 'uuid');
    }

    /**
     * user
     *
     * @return HasOne
     */
    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class, 'uuid', 'payment_uuid');
    }

    /**
     * user
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'uuid', 'category_uuid');
    }

    /**
     * user
     *
     * @return HasOne
     */
    public function orderStatus(): HasOne
    {
        return $this->hasOne(OrderStatus::class, 'uuid', 'order_status_uuid');
    }


    /** relations **/


    /**
     * return order subtotal 
     *
     * @return string
     */
    public function subtotal()
    {
        return number_format($this->amount - $this->delivery_fee, 2);
    }

    /**
     * scopeGetByDateRange
     *
     * @param  mixed  $query
     * @param  string $from
     * @param  string $to
     * @return mixed
     */
    public function scopeGetByDateRange($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to]);
    }

    public function scopeGetByUser($query, $userUUID)
    {
        return $query->where('user_uuid', $userUUID);
    }

    /**
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function orderQuery(Request $request)
    {
        $orders = self::with(['orderStatus', 'payment', 'user'])
            ->when(isset($request->from) && isset($request->to), function ($query) use ($request) {
                $query->getByDateRange($request->from, $request->to);
            })
            ->when(isset($request->uuid), function ($query) use ($request) {
                $query->getByUUID($request->uuid);
            })
            ->when(isset($request->sortBy) && in_array($request->sortBy, ['price', 'created_at']), function ($query) use ($request) {
                $query->orderBy($request->sortBy, $request->desc ? 'desc' : 'asc');
            });

        return $orders;
    }

    /**
     * getOrderListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getOrderListing(Request $request)
    {
        $orders = self::orderQuery($request)
            ->paginate($request->limit ?? 10, ['*'], 'page', request()->page);

        return $orders;
    }

    /**
     * getOrderListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getShippedOrderListing(Request $request)
    {
        $orders = self::orderQuery($request)
            ->whereHas('orderStatus', function ($query) {
                $query->where('title', 'Shipped');
            })
            ->when(isset($request->customer), function ($query) use ($request) {
                $query->whereHas('user', function ($subQuery) use ($request) {
                    $subQuery->where('first_name','like', '%' . $request->customer . '%')
                        ->orWhere('last_name','like', '%' . $request->customer . '%');
                });
            })
            ->paginate($request->limit ?? 10, ['*'], 'page', request()->page);

        return $orders;
    }

    /**
     * getUserOrderListing
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string $userUUID
     * @return mixed
     */
    public static function getUserOrderListing(Request $request, $userUUID)
    {
        $orders = self::orderQuery($request)
            ->getByUser($userUUID)
            ->paginate($request->limit ?? 10, ['*'], 'page', request()->page);

        return $orders;
    }
}
