<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid', 'title', 'slug'];

    /**** scopes ****/

    /**** scopes ****/

    /**** relations ****/
    
    /**
     * products
     *
     * @return HasMany
     */
    protected function products(): HasMany
    {
        return $this->hasMany(Product::class,'category_uuid','uuid');
    }
    
    /**** relations ****/
}
