<?php

namespace App\Models;

use App\Traits\CommonQueryTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;
    use CommonQueryTrait;

    protected $fillable = ['uuid', 'title', 'slug', 'content', 'metadata'];

    protected $casts = [
        'metadata' => 'array'
    ];

    /**** scopes ****/
    
    /**
     * scope GetByAuthor
     *
     * @param  mixed $query
     * @param  string $author
     * @return mixed
     */
    public function scopeGetByAuthor($query, string $author)
    {
        return $query->where('metadata', 'like', '%"author":"' . $author . '"%');
    }


    /**
     * getProductListing
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     */
    public static function getPostsListing(Request $request)
    {
        $products = self::when(isset($request->title), function ($query) use ($request) {
                $query->getByTitle($request->title);
            })
            ->when(isset($request->author), function ($query) use ($request) {
                $query->getByAuthor($request->author);
            })
            ->when(isset($request->sortBy) && in_array($request->sortBy, ['title']), function ($query) use ($request) {
                $query->orderBy($request->sortBy, $request->desc == 'true' ? 'desc' : 'asc');
            })
            ->paginate($request->limit ?? 20);

        return $products;
    }
}
